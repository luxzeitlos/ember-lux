import Component from '@glimmer/component';
import id from '@ember-lux/id-helper';

export default class StrictModeComponent extends Component {
  <template>
    <label for={{id "foo"}}>Foo4</label>
    <input id={{id "foo"}} />
  </template>
}
