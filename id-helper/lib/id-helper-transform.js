/* eslint-env node */
'use strict';

/*
  ```hbs
  {{id "name"}}
  ```
  becomes
  ```hbs
  {{id "hash" "name"}}
  ```

  where "hash" is a unique identifier for the template
*/

const path = require('path');

module.exports = function buildTransform(env) {
  const { syntax } = env;
  const { builders } = syntax;

  let mustWrap = false;
  function transformNode(node) {
    if (node.path.original === 'id') {
      if (node.params[0] && !node.params[0].type === 'StringLiteral') {
        node.params.shift();
      }

      if (!node.params[0] || !node.params[0].type === 'StringLiteral') {
        throw new Error(
          'the (id) helper requires at minimum one parameter and it must be a StringLiteral.' +
            path.original
        );
      }
      mustWrap = true;
      node.params.unshift(builders.path('emberLuxIdHelperContext'));
    }
  }

  function wrap(node) {
    if (mustWrap) {
      const contextComponentStr = node?.blockParams?.includes('id')
        ? 'id.contextHelper'
        : 'ember-lux-id-helper-context';

      const originalBody = node.body;
      node.body = [
        builders.block(
          'let',
          [builders.sexpr(contextComponentStr)],
          null,
          builders.blockItself(originalBody, ['emberLuxIdHelperContext'])
        ),
      ];
    }
  }

  return {
    name: 'id-helper-transform',
    visitor: {
      SubExpression: transformNode,
      MustacheStatement: transformNode,
      Template: {
        enter() {
          mustWrap = false;
        },
        exit(node) {
          if (mustWrap) {
            wrap(node);
          }
        },
      },
    },
  };
};
