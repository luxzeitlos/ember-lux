'use strict';
const makeTransform = require('./lib/id-helper-transform');

module.exports = {
  name: require('./package').name,
  setupPreprocessorRegistry(type, registry) {
    const plugin = this._buildPlugin();

    plugin.parallelBabel = {
      requireFile: __filename,
      buildUsing: '_buildPlugin',
      params: {}
    };

    registry.add('htmlbars-ast-plugin', plugin);
  },
  _buildPlugin() {
    return {
      name: 'id-helper-transform',
      plugin: makeTransform,
      baseDir() {
        return __dirname;
      },
      cacheKey: () => {
        return 'id-helper-transform';
      },
    };
  },
};
