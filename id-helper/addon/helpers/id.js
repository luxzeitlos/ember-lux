import { helper } from '@ember/component/helper';
import contextHelper from './ember-lux-id-helper-context';

const idHelper = helper(function id(params/*, hash*/) {
  return params.map(str => `i${str}`).join('-');
});

idHelper.contextHelper = contextHelper;

export default idHelper;
