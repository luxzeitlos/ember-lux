import { helper } from '@ember/component/helper';

export default helper(function id(/*params, hash*/) {
  return Math.random().toString().substr(2, 10);
});
