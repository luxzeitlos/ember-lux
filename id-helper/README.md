@ember-lux/id-helper
==============================================================================

This addons provides an `id` helper. It can be used like this:

**`components/my-component.hbs`**
```hbs
<label for={{id "firstname"}}>Firstname</label>
<input id={{id "firstname"}}>

<label for={{id "lastname"}}>Lastname</label>
<input id={{id "lastname"}}>
```

This will generate uniquie ids like `i1234-firstname` and `i1234-lastname`.
The `id` is unique to the scope. So you can have an `firstname` id in multiple
places of your app and it will never collide.

You can even call a component multiple times and each call will get
its unique id:

```hbs
<MyComponent />
<MyComponent />
```

This will then generate ids like `i1234-firstname`, `i1234-lastname`,
`i5678-firstname` and `i5678-lastname`.

## How it works

This is not necessary to know to use this addon but it can help to provide
some context.

Basically this addon wraps all your templates with a `{{#let}}`. So this:

```hbs
<label for={{id "firstname"}}>Firstname</label>
<input id={{id "firstname"}}>
```

becomes this:

```hbs
{{#let (ember-lux-id-helper-context) as |emberLuxIdHelperContext|}}
  <label for={{id emberLuxIdHelperContext "firstname"}}>Firstname</label>
  <input id={{id emberLuxIdHelperContext "firstname"}}>
{{/let}}
```

Where the `ember-lux-id-helper-context` helper just returns a random number and
the `id` helper just joins all arguments together.

Compatibility
------------------------------------------------------------------------------

* Ember.js v3.20 or above
* Ember CLI v3.20 or above
* Node.js v12 or above


Installation
------------------------------------------------------------------------------

```
ember install @ember-lux/id-helper
```


Contributing
------------------------------------------------------------------------------

See the [Contributing](CONTRIBUTING.md) guide for details.


License
------------------------------------------------------------------------------

This project is licensed under the [MIT License](LICENSE.md).
